export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyAi9A8ZI0fkjfSS2QIyP8nKxr6U-c90l6g",
    authDomain: "financyant.firebaseapp.com",
    projectId: "financyant",
    storageBucket: "financyant.appspot.com",
    messagingSenderId: "561909467772",
    appId: "1:561909467772:web:cae0ea311753be203957f9",
    measurementId: "G-08SGJLZVV2"
  },
  backendConfig: {
    apiAuth: "http://localhost:3000/auth/login",
    apiUser: "http://localhost:3000/users/",
  },
};
