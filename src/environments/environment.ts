// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAi9A8ZI0fkjfSS2QIyP8nKxr6U-c90l6g",
    authDomain: "financyant.firebaseapp.com",
    projectId: "financyant",
    storageBucket: "financyant.appspot.com",
    messagingSenderId: "561909467772",
    appId: "1:561909467772:web:cae0ea311753be203957f9",
    measurementId: "G-08SGJLZVV2"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
