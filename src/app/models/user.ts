export class User {
    id: number;
    email: string;
    password?: string;
    token: string;
    role: string;
    userName: string;
    firstName: string;
    surName: string;
    address: string;
    avatarImage: string;
    idRole: number;
    idPlan: number;
    idTypeUser: number;
    status: number;
    phonePrefix: string;
    phoneNumber: string;
    countryCode: string;
}