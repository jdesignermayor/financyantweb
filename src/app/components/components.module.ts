import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HeaderProfileComponent } from "../components/header-profile/header-profile.component";
import { LoginComponent } from "../components/login/login.component";
import { HeaderLeftOptionsComponent } from "../components/header-left-options/header-left-options.component";

@NgModule({
    entryComponents: [
        HeaderProfileComponent,
        LoginComponent,
        HeaderLeftOptionsComponent
    ],
    declarations: [
        HeaderProfileComponent,
        LoginComponent,
        HeaderLeftOptionsComponent
    ],
    exports: [
        HeaderProfileComponent,
        LoginComponent,
        HeaderLeftOptionsComponent
    ],
    imports: [CommonModule, IonicModule, ReactiveFormsModule, FormsModule],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ComponentsModule { }
