import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  public appPages = [
    { title: 'Inicio', url: '/dashboard/summary', icon: 'home-outline', iconSrc: "" },
    { title: 'Notificaciones', url: '/dashboard/notifications', icon: 'notifications-outline', iconSrc: "" },
    { title: 'Hormigas', url: '/dashboard/ants', icon: '', iconSrc: "./assets/icons/ant.svg" },
    { title: 'Ingresos', url: '/dashboard/incomings', icon: 'wallet-outline', iconSrc: "" },
    { title: 'Gastos', url: '/dashboard/expenses', icon: '', iconSrc: "./assets/icons/coins.svg", },
    { title: 'Metas', url: '/dashboard/goals', icon: '', iconSrc: "./assets/icons/goal.svg" },
  ];

  constructor(private authService: AuthService, private navCtrl: NavController) { }

  ngOnInit() {
  }

  onLogout(): void {
    this.authService.clearUserData();
    this.navCtrl.navigateRoot('/signin');
  }
}
