import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AntsPageRoutingModule } from './ants-routing.module';

import { AntsPage } from './ants.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AntsPageRoutingModule
  ],
  declarations: [AntsPage]
})
export class AntsPageModule {}
