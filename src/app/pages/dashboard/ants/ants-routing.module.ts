import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AntsPage } from './ants.page';

const routes: Routes = [
  {
    path: '',
    component: AntsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AntsPageRoutingModule {}
