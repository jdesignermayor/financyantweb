import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IncomingsPage } from './incomings.page';

const routes: Routes = [
  {
    path: '',
    component: IncomingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IncomingsPageRoutingModule {}
