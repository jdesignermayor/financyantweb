import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IncomingsPageRoutingModule } from './incomings-routing.module';

import { IncomingsPage } from './incomings.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IncomingsPageRoutingModule
  ],
  declarations: [IncomingsPage]
})
export class IncomingsPageModule {}
