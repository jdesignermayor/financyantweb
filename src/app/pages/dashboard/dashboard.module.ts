import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashboardPage } from './dashboard.page';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: DashboardPage,
    children: [
      {
        path: 'dashboard',
        children: [
          {
            path: '',
            loadChildren: './dashboard/dashboard.module#DashboardPageModule',
          },
        ],
      },
      {
        path: 'summary',
        children: [
          {
            path: '',
            loadChildren: () => import('./summary/summary.module').then(m => m.SummaryPageModule)
          },
        ],
      },
      {
        path: 'ants',
        children: [
          {
            path: '',
            loadChildren: () => import('./ants/ants.module').then(m => m.AntsPageModule)
          },
        ],
      },
      {
        path: 'incomings',
        children: [
          {
            path: '',
            loadChildren: () => import('./incomings/incomings.module').then(m => m.IncomingsPageModule)
          },
        ],
      },
      {
        path: 'expenses',
        children: [
          {
            path: '',
            loadChildren: () => import('./expenses/expenses.module').then(m => m.ExpensesPageModule)
          },
        ],
      },
      {
        path: 'goals',
        children: [
          {
            path: '',
            loadChildren: () => import('./goals/goals.module').then(m => m.GoalsPageModule)
          },
        ],
      },
      {
        path: '',
        redirectTo: '/dashboard/summary',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DashboardPage]
})
export class DashboardPageModule { }
