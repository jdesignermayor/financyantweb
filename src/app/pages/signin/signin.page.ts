import { AuthService } from '../../services/auth.service';
import { LoginApiService } from '../../services/login-api.service';
import { UserApiService } from '../../services/user-api.service';
import { User } from '../../models/user';

import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicModule, IonSlides, NavController, LoadingController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {
  @ViewChild('slideWithNav', { static: false }) slideWithNav: IonSlides;
  user: User = new User();
  confirmPassword: String;
  isActiveToggleTextPassword: Boolean = false;
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  pwdPattern = "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).{6,12}$";
  message: String;
  loading: Boolean = false;
  segmentModel: String = "login";
  eyeIcon: String = "eye";
  createAccountForm: FormGroup;

  slideOpts = {
    initialSlide: 0,
  };

  constructor(
    private auth: AuthService,
    private loginAPI: LoginApiService,
    private userAPI: UserApiService,
    private navCtrl: NavController,
    public formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createAccountForm = this.formBuilder.group({
      userName: ['', [Validators.required, Validators.pattern(this.emailPattern)]],
      firstName: ['', [Validators.required]],
      surName: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.pattern(this.pwdPattern)]],
      confirmPassword: ['', [Validators.required]],
    })
  }

  onLogin() {
    this.auth.setUserData();
    this.navCtrl.navigateRoot('/dashboard/summary');
  }

  onSubmit = async () => {
    this.loading = true;
    this.loginAPI.loginFetch(this.user.email, this.user.password).then(async (res) => {
      this.user.id = 16;
      this.user.token = res['token'];
      this.userAPI.getUserById(this.user).then(_ => {
        this.loading = false;
        this.onLogin();
      }).catch(_ => {
        this.loading = false;
      })
    }).catch(err => {
      this.message = err.message;
      this.loading = false;
    })
  }

  onCreateAccount = () => {
    if (this.createAccountForm.valid) {
      this.userAPI.createUser(this.createAccountForm.value).then((userData) => {
        console.log(userData);
        this.loading = false;
      }).catch(err => {
        console.error(err)
        this.message = err.error.message;
        this.loading = false;
      })
    }
    // if (this.user.password !== this.confirmPassword) {
    //   this.message = "Your password doesn't match the previous one";
    // } else {
    //   this.loading = true;
    //   this.userAPI.createUser(this.user).then((userData) => {
    //     console.log(userData);
    //     this.loading = false;
    //   }).catch(err => {
    //     this.message = err.error.message;
    //     this.loading = false;
    //   })
    // }
  }

  toggleTextPassword = () => {
    this.isActiveToggleTextPassword = (this.isActiveToggleTextPassword == true) ? false : true;
    if (this.eyeIcon === "eye-off-outline") {
      this.eyeIcon = "eye";
    } else {
      this.eyeIcon = "eye-off-outline";
    }
  }

  getType = () => {
    return this.isActiveToggleTextPassword ? 'text' : 'password';
  }

  segmentChanged(event) {
    console.log(this.segmentModel);
    console.log(event);
  }
}
