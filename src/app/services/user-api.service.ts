import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment.prod';
import { User } from '../models/user';
const headers = { 'Content-Type': 'application/json' };

@Injectable({
  providedIn: 'root'
})
export class UserApiService {

  constructor(private http: HttpClient) { }

  getUserById = ({ id, token }) => {
    const promise = new Promise((resolve, reject) => {
      try {
        const apiURL = env.backendConfig.apiUser;
        resolve(id);
      } catch (error) {
        reject(error);
      }
    })
    return promise;
  }

  createUser = (user: User) => {
    const promise = new Promise((resolve, reject) => {
      try {
        const apiURL = env.backendConfig.apiUser;
        this.http.post<any>(apiURL, {
          "userName": user.userName,
          "firstName": user.firstName,
          "surName": user.surName,
          "password": user.password
        }, { headers }).subscribe(data => {
          resolve(data);
        }, err => {
          reject(err)
        });
      } catch (error) {
        reject(error);
      }
    })
    return promise;
  }

}
