import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../environments/environment.prod';
const headers = { 'Content-Type': 'application/json' };

@Injectable({
  providedIn: 'root'
})
export class LoginApiService {

  constructor(private http: HttpClient) { }

  loginFetch = (username: string, password: string) => {
  
    const promise = new Promise(async (resolve, reject) => {
      try {
        const apiURL = env.backendConfig.apiAuth;
        this.http.post<any>(apiURL, {
          "username": username,
          "password": password
        }, { headers }).subscribe(data => {
          resolve(data);
        }, err => {
          reject(err.error)
        })
      } catch (error) {
        reject(error);
      }
    });
    return promise;
  }
}
